# 易大师资源监控平台
基于Jfinal3和Layui开发的 **性能测试专用** ,多主机、多服务器、多类型应用资源实时监控平台   

有问题请加QQ群：468324085  加群验证：易大师

### [更新日志](https://gitee.com/xuwangcheng/rmp_jfinal/wikis/pages?sort_id=1329102&doc_id=198597) [v0.0.3beta]

### 下载libs
非maven项目lib包下载：https://pan.baidu.com/s/1n9oHmGfIWNZZl5IfijmFpg  
maven项目需要单独下载wlfullclient.jar（weblogic11生成的jar包）http://pan.baidu.com/s/1dFxVVqh  

### Window一键安装包下载
百度云： https://pan.baidu.com/s/1SIDrXu9eoq9ppSzn8fXv6Q  
**安装完成之后先点击桌面上的 易大师资源监控平台.bat 启动，弹出的两个cmd窗口不要关闭，然后打开浏览器输入http://localhost:8080即可访问。**

### 演示站点
地址：http://www.xuwangcheng.com/rmp/login.html  
账号：请自行注册

# 安装使用说明
 **_jdk >= 1.8   
mysql >= 5.6_**    
  
1、下载或者使用git clone拷贝项目到本地；  
2、eclipse导入为maven项目或者普通项目；  
3、点击上面的百度云链接下载依赖包；  
4、使用maven下载依赖包，非maven项目将请所有从百度云下载的依赖包拷贝到项目的/src/main/webapp/WEB-INF/lib文件夹下面；  
5、将rmp.sql导入到本地数据库；    
6、修改src/main/resources下的init.properties中的数据库配置  
7、启动com.dcits包下的StartApplication类,没报错就行，访问http://localhost即可；
你也可以使用tomcat部署，需要删除libs包中的jetty-server-8.1.8.jar包  


# 系统截图
![截图1](https://images.gitee.com/uploads/images/2018/0720/161621_b34c6206_431003.png "资源监控平台1.png")
![截图2](https://images.gitee.com/uploads/images/2018/0720/161702_a3b271fb_431003.png "资源监控平台2.png")
![截图3](https://images.gitee.com/uploads/images/2018/0720/161715_a9f74e5f_431003.png "资源监控平台3.png")
![截图4](https://images.gitee.com/uploads/images/2018/0720/161725_8b44f156_431003.png "资源监控平台4.png")
![截图5](https://images.gitee.com/uploads/images/2018/0720/161737_a141ae8b_431003.png "资源监控平台5.png")
![截图6](https://images.gitee.com/uploads/images/2018/0720/161748_c2f70b7b_431003.png "资源监控平台6.png")