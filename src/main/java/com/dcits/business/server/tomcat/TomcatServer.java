package com.dcits.business.server.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.annotation.JSONField;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.server.tomcat.jmx.JMXTomcatUtil;

public class TomcatServer extends ViewServerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String SERVER_TYPE_NAME = "tomcat";
	
	private static final Logger logger = Logger.getLogger(TomcatServer.class);
	private static final Map<String, Object> alertThreshold = new HashMap<String, Object>();

	static {
		// TODO Auto-generated method stub
		alertThreshold.put("threadCurrentBusyCount", 10);
	}
	
	@JSONField(serialize=false)
	private MBeanServerConnection mbsc;
	
	public TomcatServer() {
		// TODO Auto-generated constructor stub
		super(new TomcatMonitoringInfo());
	}

	@Override
	public String connect() {
		// TODO Auto-generated method stub
		try {
			JMXTomcatUtil.getJMXConnection(this);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("tomcat-jmx连接失败", e);
			return "tomcat-jmx连接失败：" + e.getMessage() == null ? "未知原因" : e.getMessage();
		}
		return "true";
	}

	@Override
	public boolean disconect() {
		// TODO Auto-generated method stub		
		return true;
	}

	@Override
	public void getMonitoringInfo() {
		// TODO Auto-generated method stub
		JMXTomcatUtil.getMonitoringInfo(this);
	}
	
	public void setMbsc(MBeanServerConnection mbsc) {
		this.mbsc = mbsc;
	}
	
	public MBeanServerConnection getMbsc() {
		return mbsc;
	}

}
