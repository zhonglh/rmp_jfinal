package com.dcits.business.server.linux.parse;

import java.util.Map;

import com.dcits.business.server.linux.LinuxMonitoringInfo;
import com.dcits.tool.StringUtils;

public class LinuxParseInfo extends ParseInfo {
	
	protected LinuxParseInfo() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void parseNetworkInfo(String info, LinuxMonitoringInfo monitoringInfo) {
		// TODO Auto-generated method stub
		Map<String, Object> map = monitoringInfo.getNetworkInfo();
		map.put(LinuxMonitoringInfo.NETWORK_RX, GET_INFO_FAILED_MSG);
		map.put(LinuxMonitoringInfo.NETWORK_TX, GET_INFO_FAILED_MSG);
		
		if (StringUtils.isNotEmpty(info)) {
			double rx = 0, tx = 0;
			String[] strs = info.split(",");
			String[] ss = null;
			for (String s:strs) {
				ss = s.trim().split("\\s+");
				rx += Double.parseDouble(ss[0]);
				tx += Double.parseDouble(ss[1]);
			}
			map.put(LinuxMonitoringInfo.NETWORK_RX, String.format("%.2f", rx));
			map.put(LinuxMonitoringInfo.NETWORK_TX, String.format("%.2f", tx));
		}
	}

	@Override
	public void parseMountDevice(String info, LinuxMonitoringInfo monitoringInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void parseDeviceIOInfo(String info, LinuxMonitoringInfo monitoringInfo) {
		// TODO Auto-generated method stub
		Map<String, Object> map = monitoringInfo.getIoInfo();
		map.put(LinuxMonitoringInfo.IO_READ, GET_INFO_FAILED_MSG);
		map.put(LinuxMonitoringInfo.IO_WRITE, GET_INFO_FAILED_MSG);

		if (StringUtils.isNotEmpty(info)) {
			double read = 0, write = 0;
			String[] strs = info.split("\\n");
			String[] ss = null;
			
			for (String s:strs) {
				if (StringUtils.isEmpty(s)) continue;
				ss = s.trim().split("\\s+");
				read += Double.parseDouble(ss[0]);
				write += Double.parseDouble(ss[1]);				
			}			
			map.put(LinuxMonitoringInfo.IO_READ, String.format("%.2f", read));
			map.put(LinuxMonitoringInfo.IO_WRITE, String.format("%.2f", write));			
		}
	}
}
